# README #

Just clone the repo from the repo home page. Or (highly recommend this) get Git Shell if you're using Windows, or on Macintosh or any Unix based system, do a :
```
#!git

git clone https://skit_iz@bitbucket.org/skit_iz/webapps.git
```
In your desired directory.


### What is this repository for? ###

* Demo of a general website with basic CSS3 and HTML5 and JS.
* Version 1.0.0 (I guess. I'm not gonna update this more. Will maybe make it responsive. MAYBE.)

### How do I get set up? ###

* Clone repo.
* ???
* Profit.